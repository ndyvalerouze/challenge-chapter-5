const express = require('express')
const morgan = require('morgan')
const app = express()
const port = 3000

let users=[{name: "Player 1", pilihan: ""}]

app.set('view engine', 'ejs')

app.use(morgan("dev"));

app.use(express.static("public"))


app.get('/', (req, res) => {
  res.render('index')
})

app.get('/game', (req, res) => {
  res.render('game')
})

app.get('/login', (req, res) => {
  res.render('login')
})

app.get('/api/v1/users', (req, res) => {
  res.send(users)
})

app.post('/api/v1/users', (req, res) => {
  //
})

app.delete('/api/v1/users/:id', (req, res) => {
  //
})

app.put('/api/v1/users/:id', (req, res) => {
  //
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})